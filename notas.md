## Indice

1. **Introdução ao Git:**
   - O que é controle de versão?
   - Benefícios do controle de versão.
   - O que é o Git e por que é popular?

2. **Instalação e Configuração:**
   - Como instalar o Git no sistema operacional desejado.
   - Configuração inicial do Git (nome de usuário, e-mail, etc.).

3. **Comandos Básicos:**
   - `git init` - Iniciar um novo repositório.
   - `git add` - Adicionar alterações ao índice (staging area).
   - `git commit` - Confirmar as alterações.
   - `git status` - Verificar o status do repositório.
   - `git clone` - Clonar um repositório existente.

   - `git push` de um repositório existente
   ```sh
   git remote add origin https://henriqueluizz@dev.azure.com/henriqueluizz/git-apb/_git/git-apb
   ```

4. **Branching e Merging:**
   - Criar, listar e excluir branches.
   - Mudar entre branches.
   - Merging (fundindo) branches.

5. **Resolução de Conflitos:**
   - Entender e resolver conflitos durante o merge.
   - Utilização de ferramentas para resolver conflitos.

6. **Trabalhando com Repositórios Remotos:**
   - Adicionar remotos.
   - `git push` - Enviar alterações para um repositório remoto.
   - `git pull` - Atualizar o repositório local com as alterações remotas.
   - Trabalhar com branches remotas.

7. **Gitignore:**
   - Como e por que usar o arquivo .gitignore.

8. **Reverter Alterações:**
   - Desfazer alterações locais.
   - Reverter commits.

9. **Log e Histórico:**
   - Visualizar o histórico de commits.
   - Filtrar e pesquisar no histórico.

10. **Git Flow (opcional):**
    - Uma estratégia de fluxo de trabalho comum.



### GPG

Lembre-se de que a assinatura GPG é uma camada adicional de segurança, garantindo que os commits e tags sejam autênticos. Isso é especialmente valioso em ambientes de desenvolvimento colaborativo.
