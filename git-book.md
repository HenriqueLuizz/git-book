# Entendendo o Git

---

## Introdução ao Git

### O que é Controle de Versão?

O controle de versão permite o rastreamento de alterações em projetos de software, facilitando a colaboração e gerenciamento de código-fonte.

### Por que Git?

O Git é um sistema de controle de versão distribuído amplamente utilizado para o rastreamento de mudanças em projetos de software. Ele permite que várias pessoas colaborem em um projeto, gerenciando as alterações de código de maneira eficiente.

---

## Instalação e Configuração

1. **Instalando o Git:**
   - Baixe e instale o Git no [site oficial](https://git-scm.com/downloads).

2. **Configuração Inicial:**
   - Configure seu nome de usuário: `git config --global user.name "Seu Nome"`
   - Configure seu e-mail: `git config --global user.email "seu@email.com"`

## Comandos Básicos

1. **Iniciando um Repositório:**
   - `git init` - Inicia um novo repositório.

2. **Clonando um Repositório Existente:**
   - `git clone <URL>` - Clona um repositório existente.

3. **Adicionando e Confirmando Alterações:**
   - `git add <arquivo>` - Adiciona alterações ao índice (staging area).
   - `git commit -m "Mensagem de Commit"` - Confirma as alterações.

4. **Verificando o Status:**
   - `git status` - Exibe o sinmittniretatus atual do repositório.

5. **Branching e Merging:**
   - `git branch` - Lista as branches.
   - `git checkout -b <nome_branch>` - Cria e muda para uma nova branch.
   - `git merge <nome_branch>` - Funde uma branch na branch atual.
   - `git branch -d <nome_branch>` - Deleta local
   - `git push origin --delete <nome_branch>` - Deleta no repositório remoto

## Resolução de Conflitos

1. **Entendendo Conflitos:**
   - Conflitos ocorrem quando duas alterações incompatíveis são feitas no mesmo trecho do código.

2. **Resolvendo Conflitos:**
   - Utilize `git status` para identificar arquivos com conflitos.
   - Edite os arquivos manualmente para resolver conflitos.
   - Use `git add <arquivo>` após resolver cada conflito.
   - Finalize o processo com `git commit`.

## Repositórios Remotos

1. **Adicionando Remotos:**
   - `git remote add <nome_remoto> <URL>` - Adiciona um repositório remoto.

2. **Enviando e Recebendo Alterações:**
   - `git push <nome_remoto> <nome_branch>` - Envia alterações para o repositório remoto.
   - `git pull <nome_remoto> <nome_branch>` - Atualiza o repositório local com as alterações remotas.

## Gitignore

1. **Configurando o .gitignore:**
   - Crie um arquivo `.gitignore` para listar os arquivos/diretórios a serem ignorados pelo Git.

## Revertendo Alterações

1. **Desfazendo Alterações Locais:**
   - `git checkout -- <arquivo>` - Desfaz as alterações locais em um arquivo.

2. **Revertendo Commits:**
   - `git revert <hash_commit>` - Cria um novo commit revertendo as alterações de um commit específico.
   - `git reset <hash_commit>` - Desfaz commits, removendo-os do histórico.

## Log e Histórico

1. **Visualizando o Histórico:**
   - `git log` - Exibe o histórico de commits.
   - `git log --oneline` - Resumo simplificado do histórico.

---

## Git Flow

### O que é Git Flow?

O Git Flow é um modelo de fluxo de trabalho para projetos Git, proposto por Vincent Driessen. Ele define um conjunto de regras e convenções para a organização das branches em um repositório Git, facilitando o desenvolvimento colaborativo e a liberação de versões.

### Conceitos Principais do Git Flow:

1. **Branches Principais:**
   - `main`: Representa a versão de produção.
   - `develop`: É a branch de desenvolvimento principal.

2. **Branches de Recursos:**
   - `feature`: Para desenvolvimento de novas funcionalidades.
   - `release`: Para preparar a próxima versão de produção.
   - `hotfix`: Para correção de bugs em produção.

3. **Fluxo de Trabalho Básico:**
   - Crie uma branch `feature` a partir de `develop`.
   - Quando a feature estiver pronta, faça o merge de volta em `develop`.
   - Quando `develop` estiver pronto para ser lançado, crie uma branch `release`.
   - Faça o merge de `release` em `master` e `develop` após testes bem-sucedidos.
   - Branches `hotfix` são usadas para correções rápidas em produção.

---

## Regras de Pull Request (PR)

### O que é um Pull Request?

Um Pull Request é uma solicitação feita por um desenvolvedor para mesclar suas alterações em uma branch para outra. É uma prática comum para colaboração e revisão de código.

### Regras e Boas Práticas para Pull Requests:

1. **Mantenha Pull Requests Pequenos:**
   - Tente manter as mudanças em um PR relacionadas a uma única funcionalidade ou correção de bug.

2. **Forneça uma Descrição Clara:**
   - Descreva as alterações realizadas de maneira clara e concisa.

3. **Solicite Revisões:**
   - Adicione revisores ao seu PR para garantir a qualidade do código.

4. **Atenção aos Conflitos:**
   - Certifique-se de que seu PR pode ser mesclado sem conflitos.

5. **Integração Contínua:**
   - Certifique-se de que todos os testes automatizados estão passando.

6. **Mantenha o PR Atualizado:**
   - Atualize seu PR se houver alterações na branch de destino.

7. **Comentários Construtivos:**
   - Forneça e aceite feedback de maneira construtiva.

### Exemplo de Fluxo de Trabalho:

1. Crie uma branch para sua feature/bugfix.
   - `git checkout -b nome_da_feature`

2. Faça alterações e confirme.
   - `git add .`
   - `git commit -m "Mensagem descritiva"`

3. Empurre a branch para o repositório remoto.
   - `git push origin nome_da_feature`

4. Abra um Pull Request no GitHub/GitLab/Bitbucket.

5. Aguarde a revisão e feedback dos colegas.

6. Faça ajustes conforme necessário.

7. Quando aprovado, faça o merge no branch de destino.

8. Remova a branch de feature (opcional).
   - `git branch -d nome_da_feature`

---

## Boa práticas

### Revisor em pull request

Ter um revisor em um pull request é uma prática fundamental para garantir a qualidade do código e evitar a introdução de problemas no repositório.
A revisão de código é uma prática crucial para manter a qualidade do código, promover a colaboração e garantir que o repositório esteja sempre em um estado confiável. Incluir revisores em pull requests é uma maneira eficaz de aproveitar a experiência coletiva da equipe para melhorar a qualidade e robustez do software.

Aqui estão algumas razões técnicas para incluir revisores em pull requests:

1. **Identificação de Bugs e Problemas de Lógica:**
   - Revisores podem detectar possíveis bugs, erros de lógica ou abordagens subótimas que o autor do código pode ter deixado passar.

2. **Conformidade com Padrões de Codificação:**
   - Revisores garantem que o código siga os padrões de codificação definidos pela equipe. Isso promove consistência no código e facilita a manutenção.

3. **Melhoria da Legibilidade e Compreensão:**
   - Revisores podem oferecer sugestões para melhorar a legibilidade do código, tornando-o mais compreensível para outros membros da equipe.

4. **Garantia de Boas Práticas de Desenvolvimento:**
   - Revisores podem assegurar que boas práticas de desenvolvimento, como modularidade, reutilização de código e separação de preocupações, sejam seguidas.

5. **Análise de Desempenho e Eficiência:**
   - Revisores podem identificar oportunidades de otimização de desempenho e eficiência no código, garantindo que ele atenda aos requisitos de performance.

6. **Segurança do Código:**
   - Revisores desempenham um papel importante na identificação de potenciais vulnerabilidades de segurança no código. Eles podem ajudar a prevenir a introdução de código malicioso ou vulnerável.

7. **Conhecimento Compartilhado:**
   - Revisores podem compartilhar conhecimento e experiência, promovendo a aprendizagem contínua da equipe. Isso é especialmente valioso em equipes com membros com diferentes níveis de experiência.

8. **Validação de Testes e Documentação:**
   - Revisores podem verificar se os testes foram devidamente implementados e se a documentação foi atualizada. Isso contribui para a confiabilidade do código.

9. **Garantia da Integração Contínua:**
   - Revisores podem assegurar que o código passou pelos processos de integração contínua e que os testes automatizados foram executados com sucesso.

10. **Tomada de Decisões Informadas:**
    - A revisão de código promove uma abordagem colaborativa, permitindo que vários membros da equipe contribuam para decisões de design e arquitetura.

11. **Prevenção de Conflitos e Erros de Merge:**
    - A revisão antecipada ajuda a evitar conflitos e problemas durante o merge, garantindo uma transição mais suave do código para o repositório principal.

---

## Configuração de Commits Seguros com GPG

Configurar o `signingkey` e utilizar o GPG (GNU Privacy Guard) para realizar commits seguros é uma prática importante, especialmente quando se trata de garantir a autenticidade e integridade das alterações no código.

### Configurando a Chave GPG

1. **Instalação do GPG:**
   - Certifique-se de que o GPG está instalado no seu sistema. Se não estiver instalado, baixe e instale a versão adequada para o seu sistema operacional.

2. **Geração de uma Chave GPG:**
   - Execute o seguinte comando para gerar uma nova chave GPG. Substitua `<seu-email>` pelo seu endereço de e-mail.
     ```bash
     gpg --full-generate-key
     ```
   - Siga as instruções para configurar a chave. Lembre-se da passphrase que você define.

3. **Listando Chaves GPG:**
   - Liste as chaves GPG existentes no seu sistema.
     ```bash
     gpg --list-secret-keys --keyid-format LONG
     ```
   - Anote o ID da chave (normalmente uma sequência alfanumérica após `sec`).

4. **Exportando a Chave Pública GPG:**
   - Exporte a chave pública GPG para o uso no Git.
     ```bash
     gpg --armor --export <ID_DA_CHAVE>
     ```

5. **Configurando a Chave GPG no Git:**
   - Configure o Git para usar a chave GPG para commits.
     ```bash
     git config --global user.signingkey <ID_DA_CHAVE>
     ```

### Utilizando a Chave GPG para Commits Seguros

6. **Assinando Commits Localmente:**
   - Ao realizar um commit, adicione a opção `-S` para assinar o commit.
     ```bash
     git commit -S -m "Mensagem do Commit"
     ```

7. **Assinando Todos os Commits Automaticamente:**
   - Configure o Git para assinar todos os seus commits automaticamente.
     ```bash
     git config --global commit.gpgSign true
     ```

8. **Verificando Assinaturas Locais:**
   - Verifique localmente as assinaturas de commits.
     ```bash
     git log --show-signature
     ```

9. **Assinando Tags:**
   - Ao criar uma tag, use a opção `-s` para assinar a tag.
     ```bash
     git tag -s <nome_da_tag>
     ```

10. **Enviando a Chave Pública GPG para Plataformas de Hospedagem:**
   - Em plataformas como GitHub ou Azure DevOps, adicione a chave pública GPG ao seu perfil de usuário.

11. **Configurando Assinatura Global para Todos os Repositórios:**
   - Configure a assinatura global para todos os seus repositórios Git.
      ```bash
      git config --global commit.gpgSign true
      ```

---

### Git Básico:

1. **Pergunta:** Qual é a principal função do Git?
   - **Resposta:** O Git é um sistema de controle de versão distribuído que rastreia as alterações em arquivos e facilita o trabalho colaborativo em projetos de software.

2. **Pergunta:** Como você inicia um novo repositório Git?
   - **Resposta:** Utilizando o comando `git init` em um diretório.

3. **Pergunta:** Explique a diferença entre `git add` e `git commit`.
   - **Resposta:** `git add` adiciona alterações ao índice (staging area), e `git commit` confirma essas alterações no repositório.

### Git Flow:

4. **Pergunta:** Qual é o propósito principal do Git Flow?
   - **Resposta:** O Git Flow é um modelo de fluxo de trabalho que fornece um conjunto de regras e convenções para organizar branches, facilitando o desenvolvimento colaborativo e a liberação de versões.

5. **Pergunta:** Quais são as branches principais no Git Flow?
   - **Resposta:** `master` (para produção) e `develop` (para desenvolvimento).

6. **Pergunta:** Como você cria uma nova feature no Git Flow?
   - **Resposta:** Utilizando o comando `git checkout -b feature/nome_da_feature`.

### Boas Práticas de Commits Seguros com GPG:

7. **Pergunta:** Por que é importante assinar commits com GPG?
   - **Resposta:** A assinatura GPG garante a autenticidade e integridade dos commits, proporcionando um nível adicional de segurança.

8. **Pergunta:** Como você configura a chave GPG no Git?
   - **Resposta:** Utilizando o comando `git config --global user.signingkey <ID_DA_CHAVE>`.

9. **Pergunta:** Qual é a diferença entre `git commit -m` e `git commit -S -m`?
   - **Resposta:** `git commit -S -m` assina o commit usando GPG, enquanto `git commit -m` não assina.

10. **Pergunta:** Como você verifica localmente as assinaturas de commits?
   - **Resposta:** Utilizando o comando `git log --show-signature`.

Boas práticas de nomenclatura de repositório e estrutura de projeto são fundamentais para garantir uma organização clara e eficiente, especialmente ao integrar automação com CI/CD (Integração Contínua/Entrega Contínua).

### Nomenclatura de Repositório:

1. **Nome Descritivo:**
   - Escolha um nome que descreva claramente o propósito ou o conteúdo do repositório.

2. **Padrão de Nomenclatura:**
   - Mantenha um padrão consistente para todos os repositórios, facilitando a identificação e compreensão.

3. **Evite Abreviações Obscuras:**
   - Evite abreviações que não sejam amplamente reconhecidas, pois isso pode dificultar a compreensão do propósito do repositório.

4. **Utilize Hífens ou Camel Case:**
   - Use hífens ou camel case para separar palavras no nome do repositório. Isso melhora a legibilidade.

### Estrutura de Projeto:

5. **Organização Lógica:**
   - Estruture o projeto de forma lógica, agrupando componentes relacionados em diretórios.

6. **Padrões de Nome de Diretório:**
   - Estabeleça padrões consistentes para os nomes de diretórios, como "src" para código-fonte, "docs" para documentação, etc.

7. **Separar Artefatos Build:**
   - Mantenha artefatos de compilação e outros arquivos gerados separados do código-fonte para evitar poluição do repositório.

8. **Arquivos de Configuração na Raiz:**
   - Coloque os arquivos de configuração fundamentais (como arquivos de CI/CD) na raiz do projeto para fácil acesso.
