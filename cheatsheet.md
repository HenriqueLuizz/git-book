
# Cheat Sheet: Git no Azure DevOps

## Configuração Inicial

1. **Clone um Repositório:**
   ```bash
   git clone https://<sua-organizacao>.visualstudio.com/<seu-projeto>/_git/<seu-repositorio>
   ```

2. **Configuração do Git com o Azure DevOps:**
   ```bash
   git config --global user.name "Seu Nome"
   git config --global user.email "seu@email.com"
   git config --global credential.helper wincred # Para salvar credenciais no Windows
   ```

## Trabalhando com Branches

3. **Criar uma Nova Branch:**
   ```bash
   git checkout -b nome_da_branch
   ```

4. **Listar Branches Locais e Remotas:**
   ```bash
   git branch
   git branch -r
   ```

5. **Trocar de Branch:**
   ```bash
   git checkout nome_da_branch
   ```

6. **Push de uma Nova Branch:**
   ```bash
   git push origin nome_da_branch
   ```

## Commits e Mudanças

7. **Adicionar Mudanças para o Commit:**
   ```bash
   git add .
   git add nome_do_arquivo.txt
   ```

8. **Commitar Mudanças:**
   ```bash
   git commit -m "Mensagem descritiva"
   ```

9. **Push de Commits para o Repositório Remoto:**
   ```bash
   git push origin nome_da_branch
   ```

## Pull e Merge

10. **Atualizar o Repositório Local:**
    ```bash
    git pull origin nome_da_branch
    ```

11. **Fazer Merge de uma Branch:**
    ```bash
    git checkout branch_destino
    git merge nome_da_branch
    ```

## Resolução de Conflitos

12. **Verificar por Conflitos:**
    ```bash
    git status
    ```

13. **Resolver Conflitos Manualmente e Commitar:**
    ```bash
    git add .
    git commit -m "Resolva conflitos"
    ```

## Azure DevOps - Pull Request

14. **Abrir um Pull Request:**
    - No repositório do Azure DevOps, vá para "Pull Requests" e clique em "New Pull Request".

15. **Comentar e Revisar um Pull Request:**
    - Adicione revisores e comente no Azure DevOps.

16. **Completar um Pull Request:**
    - No Azure DevOps, após a revisão, clique em "Complete" para mesclar.

17. **Remover Branch Após Merge:**
    ```bash
    git branch -d nome_da_branch
    ```

## Comandos Gerais

18. **Histórico de Commits:**
    ```bash
    git log
    ```

19. **Desfazer Último Commit (Local):**
    ```bash
    git reset HEAD~1 --soft
    ```

20. **Desfazer Último Commit (Local e Remoto):**
    ```bash
    git push origin +HEAD
    ```

---